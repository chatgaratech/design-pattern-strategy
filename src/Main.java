import paiement.Commande;
import paiement.impl.CarteCreditStrategy;
import paiement.impl.PaypalStrategy;
import paiement.impl.VirementBancaireStrategy;

public class Main {
    public static void main(String[] args) {

        Commande commande1 = new Commande(new CarteCreditStrategy("XXXXX", "XXXXX"));
        commande1.traiterPaiement(100);

        Commande commande2 = new Commande(new PaypalStrategy("fdsfdsf", "motpasse"));
        commande2.traiterPaiement(200);

        Commande commande3 = new Commande(new VirementBancaireStrategy("IBAN"));
        commande3.traiterPaiement(300);
    }
}