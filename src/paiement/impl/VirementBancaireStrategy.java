package paiement.impl;

import paiement.PaiementStrategy;

public class VirementBancaireStrategy implements PaiementStrategy {
    private String iban;

    public VirementBancaireStrategy(String iban) {
        this.iban = iban;
    }

    @Override
    public void payer(int montant) {
        System.out.println(montant + " payé par virement bancaire");
    }
}
