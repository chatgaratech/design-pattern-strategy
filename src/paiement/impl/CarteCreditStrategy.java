package paiement.impl;

import paiement.PaiementStrategy;

public class CarteCreditStrategy implements PaiementStrategy {
    private String numeroCarte;
    private String cvv;

    public CarteCreditStrategy(String numeroCarte, String cvv) {
        this.numeroCarte = numeroCarte;
        this.cvv = cvv;
    }

    @Override
    public void payer(int montant) {
        System.out.println(montant + " payé avec la carte de crédits");
    }
}
