package paiement;

public interface PaiementStrategy {
    void payer(int montant);
}
