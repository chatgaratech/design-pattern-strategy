package paiement;

public class Commande {
    private final PaiementStrategy paiementStrategy;

    public Commande(PaiementStrategy paiementStrategy) {
        this.paiementStrategy = paiementStrategy;
    }

    public void traiterPaiement(int montant) {
        this.paiementStrategy.payer(montant);
    }
}
