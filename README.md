# design-pattern-strategy

Description

* Exemple simple sur la création d'une strategy (Design Pattern Strategy)

Auteur

* Chatgara-Tech

Chaine Youtube :

* https://www.youtube.com/@Chatgara-Tech


Sources :

* https://refactoring.guru/design-patterns/strategy